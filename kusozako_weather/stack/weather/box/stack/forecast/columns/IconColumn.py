
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_weather import WeatherModelColumnTypes

RESOURCE_NAME_TEMPLATE = ".weather_icons/svg/{}.svg"


class DeltaIconColumn(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, tree_iter, user_data):
        tree_row = model[tree_iter]
        symbol_code = tree_row[WeatherModelColumnTypes.SYMBOL_CODE]
        resource_name = RESOURCE_NAME_TEMPLATE.format(symbol_code)
        path = self._enquiry("delta > resource path", resource_name)
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        renderer.set_property("pixbuf", pixbuf)

    def _delta_info_tree_view_column(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            title=_("Weather")
            )
        self._raise("delta > insert column", self)
        self.set_cell_data_func(renderer, self._cell_data_func)
