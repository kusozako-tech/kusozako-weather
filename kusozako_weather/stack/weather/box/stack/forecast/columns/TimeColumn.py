
from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_weather import WeatherModelColumnTypes


class DeltaTimeColumn(Gtk.TreeViewColumn, DeltaEntity):

    def _cell_data_func(self, column, renderer, model, tree_iter, user_data):
        pass

    def _delta_info_tree_view_column(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(
            xpad=Unit(1),
            ellipsize=Pango.EllipsizeMode.MIDDLE,
            xalign=0,
            )
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            title=_("Time"),
            text=WeatherModelColumnTypes.READABLE_DATE_TIME
            )
        self.set_expand(True)
        # self.set_sort_column_id(LocationSearchColumnTypes.LOCATION_NAME)
        self._raise("delta > insert column", self)
        self.set_cell_data_func(renderer, self._cell_data_func)
