
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .columns.TimeColumn import DeltaTimeColumn
from .columns.IconColumn import DeltaIconColumn
from .columns.DescriptionColumn import DeltaDescriptionColumn


class DeltaForecast(Gtk.TreeView, DeltaEntity):

    def _delta_call_insert_column(self, column):
        self.append_column(column)

    def _delta_info_tree_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.TreeView.__init__(self, model=self._enquiry("delta > model"))
        scrolled_window.add(self)
        DeltaTimeColumn(self)
        DeltaIconColumn(self)
        DeltaDescriptionColumn(self)
        data = scrolled_window, "forecast"
        self._raise("delta > add to stack named", data)
