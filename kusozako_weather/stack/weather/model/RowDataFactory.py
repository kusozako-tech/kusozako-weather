
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaRowDataFactory(DeltaEntity):

    def _append_data(self, time_zone, time_series, key):
        date_time = GLib.DateTime.new_from_iso8601(
            time_series["time"],
            time_zone
            ).to_local()
        details = time_series["data"]["instant"]["details"]
        next_ = time_series["data"][key]
        temperature_text = "{} °C".format(details["air_temperature"])
        precipitation_float = next_["details"]["precipitation_amount"]
        precipitation_text = "{} mm".format(precipitation_float)
        tree_row_data = (
            date_time,
            date_time.format("%F(%a)\n%R"),
            details["air_pressure_at_sea_level"],
            details["air_temperature"],
            details["cloud_area_fraction"],
            details["relative_humidity"],
            details["wind_from_direction"],
            details["wind_speed"],
            next_["summary"]["symbol_code"],
            next_["details"]["precipitation_amount"],
            temperature_text+"\n"+precipitation_text
            )
        self._raise("delta > append tree row data", tree_row_data)

    def construct_data(self, time_zone, time_series):
        if "next_1_hours" in time_series["data"]:
            self._append_data(time_zone, time_series, "next_1_hours")
        elif "next_6_hours" in time_series["data"]:
            self._append_data(time_zone, time_series, "next_6_hours")

    def __init__(self, parent):
        self._parent = parent
