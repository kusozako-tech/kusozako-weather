
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# from .TypeColumn import DeltaTypeColumn
from .NameColumn import DeltaNameColumn
from .LatitudeColumn import DeltaLatiudeColumn
from .LongitudeColumn import DeltaLongitudeColumn


class EchoColumns:

    def __init__(self, parent):
        # DeltaTypeColumn(parent)
        DeltaNameColumn(parent)
        DeltaLatiudeColumn(parent)
        DeltaLongitudeColumn(parent)
