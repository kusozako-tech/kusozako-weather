
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import TimezoneMap
from libkusozako3.Entity import DeltaEntity


class DeltaTimezoneMap(TimezoneMap.TimezoneMap, DeltaEntity):

    def _on_location_changed(self, timezone_map, timezone_location):
        pass
        # offset = timezone_map.get_selected_offset()
        # zone_name = timezone_location.get_zone()

    def __init__(self, parent):
        self._parent = parent
        TimezoneMap.TimezoneMap.__init__(self)
        self.connect("location-changed", self._on_location_changed)
        self._raise("delta > add to container", self)
