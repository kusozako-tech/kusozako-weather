
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _on_activate(self, entry):
        text = entry.get_text()
        if text:
            self._raise("delta > keyword changed", text)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(self)
        self.connect("activate", self._on_activate)
        self._raise("delta > add to container", self)
