
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .TimezoneMap import DeltaTimezoneMap
from .SearchEntry import DeltaSearchEntry
from .tree_view.TreeView import DeltaTreeView


class DeltaUI(Gtk.Box, DeltaEntity):

    def _delta_call_selection_changed(self, user_data):
        location_name, latitude, longitude = user_data
        self._timezone_map.set_location(longitude, latitude)
        location = self._timezone_map.get_location()
        settings = {
            "location-name": location_name,
            "zone-name": location.get_zone(),
            "time-offset": self._timezone_map.get_selected_offset(),
            "latitude": latitude,
            "longitude": longitude
            }
        self._raise("delta > settings", ("weather", "current", settings))

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            spacing=Unit(1)
            )
        self._timezone_map = DeltaTimezoneMap(self)
        DeltaSearchEntry(self)
        DeltaTreeView(self)
        self._raise("delta > add to stack named", (self, "location-search"))
