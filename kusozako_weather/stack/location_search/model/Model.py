
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_weather import LocationSearchColumnTypes
from .OsmNominatim import DeltaOsmNominatim


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_model_data(self, model_data):
        for node in model_data:
            type_ = node["type"]
            display_name = node["display_name"]
            latitude = float(node["lat"])
            longitude = float(node["lon"])
            self.append((type_, display_name, latitude, longitude))
        self._loading = False

    def get_is_loading(self):
        return self._loading

    def set_keyword(self, text):
        self._loading = True
        self.clear()
        self._osm_nominatim.search(text)

    def __init__(self, parent):
        self._parent = parent
        self._loading = True
        Gtk.ListStore.__init__(self, *LocationSearchColumnTypes.TYPES)
        self._osm_nominatim = DeltaOsmNominatim(self)
        self._osm_nominatim.search("nishinomiya")
