
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import deque
from libkusozako3.Entity import DeltaEntity
from .SoupSession import DeltaSoupSession


class DeltaApi(DeltaEntity):

    def _get_nodes(self, xml_string):
        pass

    def _get_api(self, query):
        pass

    def _raise_finished(self, model_data):
        if model_data:
            self._raise("delta > model data", model_data)

    def _delta_call_soup_session_finished(self, xml_string):
        yuki_model_data = deque()
        for yuki_node in self._get_nodes(xml_string):
            yuki_model_data.append(yuki_node.attrib)
        self._raise_finished(yuki_model_data)

    def _delta_call_session_error(self, user_data):
        pass

    def search(self, query):
        yuki_api = self._get_api(query)
        self._soup_session.get_async(yuki_api)

    def _post_initialization(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._soup_session = DeltaSoupSession(self)
        self._post_initialization()
