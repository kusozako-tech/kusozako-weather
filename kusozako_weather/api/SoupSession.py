
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from gi.repository import Gio
from gi.repository import Soup
from libkusozako3.Entity import DeltaEntity


class DeltaSoupSession(DeltaEntity):

    def _read_input_stream(self, input_stream):
        yuki_data_input_stream = Gio.DataInputStream.new(input_stream)
        yuki_data = ""
        while True:
            yuki_line, _ = yuki_data_input_stream.read_line_utf8()
            if yuki_line is None:
                break
            yuki_data += yuki_line
        self._raise("delta > soup session finished", yuki_data)

    def _on_finished(self, session, result, message):
        yuki_input_stream = session.send_finish(result)
        if yuki_input_stream:
            self._read_input_stream(yuki_input_stream)
        else:
            self._raise("delta > soup session error", message.status_code)

    def _send_async(self, message):
        self._session.send_async(message, None, self._on_finished, message)

    def get_async(self, uri):
        # must run on another thread.
        yuki_message = Soup.Message.new("GET", uri)
        GLib.idle_add(self._send_async, yuki_message)

    def __init__(self, parent):
        self._parent = parent
        self._session = Soup.Session()
        yuki_user_agent = self._enquiry("delta > soup session user agent")
        if yuki_user_agent is not None:
            self._session.set_property("user-agent", yuki_user_agent)
