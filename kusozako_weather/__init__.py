
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale
import gi

gi.require_version('TimezoneMap', '1.0')

VERSION = "2022.10.14"
APPLICATION_NAME = "kusozako-weather"
GROUP_ID = "com.gitlab.kusozako-tech"
APPLICATION_ID = "{}.{}".format(GROUP_ID, APPLICATION_NAME)

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

LONG_DESCRIPTION = _("""weather forecast application.
This software is licencced under GPL version 3 or any later version.""")

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "version": VERSION,
    "short description": "Weather forecast",
    "long-description": LONG_DESCRIPTION
    }
