
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_weather import LocationSearchColumnTypes
from .columns.Columns import EchoColumns


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_insert_column(self, column):
        self.append_column(column)

    def _delta_info_tree_view(self):
        return self

    def _on_changed(self, selection):
        if self._enquiry("delta > model is loading"):
            return
        model, tree_iter = selection.get_selected()
        if tree_iter is None:
            return
        tree_row = model[tree_iter]
        location_name = tree_row[LocationSearchColumnTypes.LOCATION_NAME]
        latitude = tree_row[LocationSearchColumnTypes.LATITUDE]
        longitude = tree_row[LocationSearchColumnTypes.LONGITUDE]
        data = location_name, latitude, longitude
        self._raise("delta > selection changed", data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.TreeView.__init__(self, vexpand=True, hexpand=True)
        self.set_model(self._enquiry("delta > model"))
        self.set_headers_visible(True)
        self.set_tooltip_column(LocationSearchColumnTypes.LOCATION_NAME)
        selection = self.get_selection()
        selection.connect("changed", self._on_changed)
        EchoColumns(self)
        self._raise("delta > add to container", self)
