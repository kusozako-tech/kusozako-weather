
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .weather.Weather import DeltaWeather
from .location_search.LocationSearch import DeltaLocationSearch


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_main_stack_to(self, name):
        self.set_visible_child_name(name)

    def _set_initial_stack(self):
        query = "weather", "current", None
        settings = self._enquiry("delta > settings", query)
        if settings is not None:
            self._raise("delta > settings", ("weather", "current", settings))
        else:
            self.set_visible_child_name("location-search")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self, hexpand=True, vexpand=True)
        label = Gtk.Label("Please Wait...")
        self._raise("delta > css", (label, "toolbar"))
        self.add_named(label, "please-wait")
        DeltaLocationSearch(self)
        DeltaWeather(self)
        self._raise("delta > add to container", self)
        self._set_initial_stack()
