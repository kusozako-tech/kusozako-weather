
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity

ICON_NAME = "edit-find-symbolic"
ICON_SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaAction(DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > switch main stack to", "location-search")

    def _add_action_widget(self):
        button = Gtk.Button(
            relief=Gtk.ReliefStyle.NONE,
            image=Gtk.Image.new_from_icon_name(ICON_NAME, ICON_SIZE)
            )
        button.connect("clicked", self._on_clicked)
        self._raise("delta > css", (button, "popover-button"))
        self._raise("delta > add action widget start", button)

    def __init__(self, parent):
        self._parent = parent
        self._add_action_widget()
