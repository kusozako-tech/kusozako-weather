
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .model.Model import DeltaModel
from .ui.UI import DeltaUI
from .Action import DeltaAction


class DeltaLocationSearch(DeltaEntity):

    def _delta_call_keyword_changed(self, text):
        self._model.set_keyword(text)

    def _delta_info_model_is_loading(self):
        return self._model.get_is_loading()

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        DeltaUI(self)
        DeltaAction(self)
