
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity


class DeltaLocationLabel(Gtk.Label, DeltaEntity):

    def set_model(self, model):
        query = "weather", "current", None
        current_settings = self._enquiry("delta > settings", query)
        location_name = current_settings["location-name"]
        self.set_label(location_name)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(
            self,
            hexpand=True,
            wrap=True,
            wrap_mode=Pango.WrapMode.WORD_CHAR,
            justify=Gtk.Justification.CENTER
            )
        geometries = 0, 2, 2, 1
        self._raise("delta > attach to grid", (self, geometries))
