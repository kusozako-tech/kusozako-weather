
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import json
from gi.repository import GLib
from kusozako_weather.api.Api import DeltaApi

BASE_API = "https://api.met.no/weatherapi/locationforecast/"
VERSION = "2.0"
QUERY = "compact?lat={}&lon={}"
USER_AGENT = "kusozako-weather/{} gitlab.com/kusozako-tech"


class DeltaMetWeatherApi(DeltaApi):

    def _delta_info_soup_session_user_agent(self):
        version = self._enquiry("delta > application data", "version")
        return USER_AGENT.format(version)

    def _get_api(self, query):
        api = GLib.build_filenamev([BASE_API, VERSION, QUERY])
        return api.format(*query)

    def _delta_call_soup_session_finished(self, json_string):
        json_dict = json.loads(json_string)
        self._raise("delta > soup session finished", json_dict)

    def receive_transmission(self, user_data):
        group, key, value = user_data
        if group != "weather" or key != "current":
            return
        data = value["latitude"], value["longitude"]
        self.search(data)

    def _post_initialization(self):
        self._raise("delta > register settings object", self)
