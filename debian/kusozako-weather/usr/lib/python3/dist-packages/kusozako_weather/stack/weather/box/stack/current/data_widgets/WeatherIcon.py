
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_weather import WeatherModelColumnTypes as Column

RESOURCE_NAME_TEMPLATE = ".weather_icons/svg/{}.svg"


class DeltaWeatherIcon(Gtk.Image, DeltaEntity):

    def set_model(self, tree_row):
        symbol_code = tree_row[Column.SYMBOL_CODE]
        resource_name = RESOURCE_NAME_TEMPLATE.format(symbol_code)
        path = self._enquiry("delta > resource path", resource_name)
        self.set_from_file(path)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, hexpand=True)
        geometries = 0, 0, 2, 1
        self._raise("delta > attach to grid", (self, geometries))
