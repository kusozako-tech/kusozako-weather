
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_weather import WeatherModelColumnTypes
from .MetWeatherApi import DeltaMetWeatherApi
from .RowDataFactory import DeltaRowDataFactory


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append_tree_row_data(self, tree_row_data):
        self.append(tree_row_data)

    def _delta_call_soup_session_finished(self, json_dict):
        self.clear()
        query = "weather", "current", ""
        current_settings = self._enquiry("delta > settings", query)
        time_zone = GLib.TimeZone.new(current_settings["zone-name"])
        for time_series in json_dict["properties"]["timeseries"]:
            self._row_data_factory.construct_data(time_zone, time_series)
        self._raise("delta > switch main stack to", "weather")

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *WeatherModelColumnTypes.TYPES)
        self._row_data_factory = DeltaRowDataFactory(self)
        DeltaMetWeatherApi(self)
