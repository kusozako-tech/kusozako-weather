
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

TYPES = (
    str,
    str,
    float,
    float
    )

LOCATION_TYPE = 0
LOCATION_NAME = 1
LATITUDE = 2
LONGITUDE = 3

N_COLUMNS = 4
