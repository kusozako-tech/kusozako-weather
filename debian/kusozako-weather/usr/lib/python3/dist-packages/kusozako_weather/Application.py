
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.application.Application import AlfaApplication
from . import APPLICATION_DATA
from .CommandLine import OPTIONS
from .stack.Stack import DeltaStack


class DeltaApplication(AlfaApplication):

    def _delta_call_loopback_start_application(self, parent):
        DeltaStack(parent)

    def _delta_info_command_line_options(self):
        return OPTIONS

    def _delta_info_application_data(self, key):
        return APPLICATION_DATA[key] if key in APPLICATION_DATA else None

    def _delta_info_application_library_directory(self):
        return GLib.path_get_dirname(__file__)

    def _request_synchronization(self, message, user_data=None):
        print("delta > message uncaught", message, user_data)
