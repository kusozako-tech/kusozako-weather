
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SEARCH = "search"
CURRENT = "current"
FORECAST = "forecast"


DEFAULT_SETTING = "view", "stack_name", SEARCH
